<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-tradrub?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tradrub_description' => 'Dieses Plugin fügt den Rubriken dem Übsersetzungssystem der Artikel nachempfundene Funktionen hinzu: Ein neues Feld id_trad verbindet die Übersetzungen von Rubriken.',
	'tradrub_nom' => 'Übersetzungen der Rubriken',
	'tradrub_slogan' => 'Verwaltung von Übersetzungslinks zwischen Rubriken'
);
