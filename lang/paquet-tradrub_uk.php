<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-tradrub?lang_cible=uk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tradrub_description' => 'Цей плагін додає систему, подібну до перекладів статей: він додає поле id_trad у таблицю rubriques і пов’язує між собою переклади розділів.',
	'tradrub_nom' => 'Tradrub. Управління перекладами розділів',
	'tradrub_slogan' => 'Активує систему управління перекладами для розділів'
);
