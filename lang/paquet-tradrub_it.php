<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-tradrub?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tradrub_description' => 'Questo plugin aggiunge un sistema ispirato a quello delle traduzioni degli articoli: aggiunge un campo id_trad alla tabella delle intestazioni e quindi collega le traduzioni delle intestazioni.',
	'tradrub_nom' => 'Traduzione tra rubriche',
	'tradrub_slogan' => 'Gestione dei collegamenti di traduzione tra le rubriche'
);
